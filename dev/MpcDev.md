[TOC]



## 1、前言

架构评审或技术方案评审的价值在于集众人的力量大家一起来分析看看方案里是否有坑，方案上线后是否会遇到不可逾越的重大技术问题，提前尽可能把一些事情先考虑到，最终能够对团队和公司的健康发展有很大的好处。



## 2、术语

- 组件 

  > 开源的 Java 工具，或者开源的服务应用

- 原则

  > 遵守的规范



## 3、选型原则

### 3.1 安全性

- 拖库，内存泄漏
- 是否植入非法代码
- SQL注入和XSS
- DDOS攻击
- 机密数据加密
- ...

### 3.2 合规/法性（开源协议，商业组件开源限制）	

遵守协议，避免商用麻烦

#### 3.2.1 开源协议

##### 许可协议

​	什么是许可，当你为你的产品签发许可，你是在出让自己的权利，不过，你仍然拥有版权和专利（如果申请了的话），许可的目的是，向使用你产品的人提供 一定的权限。

不管产品是免费向公众分发，还是出售，制定一份许可协议非常有用，否则，对于前者，你相当于放弃了自己所有的权利，任何人都没有义务表明你的原始作 者身份，对于后者，你将不得不花费比开发更多的精力用来逐个处理用户的授权问题

##### 5 大许可协议

**GPL**

代码开源/免费使用和引用/修改/衍生代码的开源/免费使用，但不允许修改后和衍生的代码做为闭源的商业软件发布和销售

> 保密性高的公司不适合二次开发

**BSD**

允许使用者修改和重新发布代码，也允许使用或在BSD代码上开发商业软件发布和销售

>鼓励代码共享，但需要尊重代码作者的著作权
>
>必要的时候可以修改或者二次开发

**MIT**

作者只想保留版权,而无任何其他了限制.

>你必须在你的发行版里包含原许可协议的声明,无论你是以二进制发布的还是以源代码发布的

**MPL** `Mozilla`

经MPL许可证发布的源代码的修改也要以MPL许可证的方式再许可出来，以保证其他人可以在MPL的条款下共享源代码

> MPL允许一个企业在自己已有的源代码库上加一个接口，除了接口程序的源代码以MPL 许可证的形式对外许可外，源代码库中的源代码就可以不用MPL许可证的方式强制对外许可

**Apache**

该协议和BSD类似，同样鼓励代码共享和尊重原作者的著作权，同样允许代码修改，再发布（作为开源或商业软件）

>可修改， 在延伸的代码中（修改和有源代码衍生的代码中）需要带有原来代码中的协议

**LGPL**

​	允许商业软件通过类库引用(link)方式使用LGPL类库而不需要开源商业软件的代码。

>适合作为第三方类库被商业软件引用
>
>不适合二次开发的商业软件采用

#### 3.2.2 开源限制

 一张图即可

![](http://10.20.13.197:4999/server/index.php?s=/api/attachment/visitFile/sign/39ec096e9b9150cbd7ecf2abb0e40368&showdoc=.jpg)



#### 3.2.2 协议选择

Apache  ==>  MIT  ==> BSD ==> GPL ==> Mozilla  ==>  LGPL 



### 3.3 可维护性（文档、社区活跃、成熟度）

#### 3.3.1 文档

- **简单**

  > 能够按照文档快速使用
  >
  > 最好能够有在线使用使用样例

- **功能明确**

  > 是否符合自己的需求

- **最新**

  > 文档是最新的

#### 3.3.2 热度 

- **开源社区**

  > 功能升级
  >
  > 社区问题

- **github（gitee）start**

  >代码更新频率、最新更新时间 。（保证代码有人维护和升级）
  >
  >代码下载量、start、flow ...

#### 3.3.3 FAQ issue

- **已解决的问题**

  >避免自己踩坑，学习经验

- **未解决的问题**

  > 是否影响自己的业务需求，提早防范



### 3.4 POC (相关组件的性能测试)

#### 3.4.1 功能

- **准确性**

  > 满足自己的功能需求，输入输出 一定能够达到我们预想的

- **稳定性**

  > 业务流程中，能够满足各种情况

#### 3.4.2 性能

- **压测试**

  > 高压状态下多用户高并发测试（30万-50万），主要关注系统是怎么崩溃的。（内存泄漏，cpu无响应，数据库无反应，网络堵塞）

- **容量测**：

  > 系统最大支撑的相关数量，数据库最大数据数量，用户数量。



## 4、管理（如：Nexus公司专门repository、统一pom管理）

### 4.1 私服

公司使用的私服是Nexus，具体地址

- **内网**

  http://10.20.13.191:8081

- **外网**

  http://221.226.40.56:5066

### 4.2 权限

#### 4.2.1开发者

负责管理开源组件和收集共性问题完善内部组件

**统一版本管理**pom

> 比如 springcloud、gson、jdbc ... 版本
>
> 工程maven编码、规则、profile、私服地址 ...

**更新开源组件**

**发布内部组件**

> 缓存、国际化、安全  ...

**更新内部组件**

> 依据使用者提出问题，升级组件

#### 4.2.2使用者

各项目开发使用者

**通过统一pom，引入项目所需要的包**

> 项目导入依赖，无需指定版本。只要从统一pom中挑选就行

**使用**

> 引入依赖，构建，调用API

**提问题**

> bug或者公共功能提出，提给框架组进行处理。

### 4.3 发布

就是把组件发布到私服中。提供给各个项目组使用

#### 4.3.1 发布注意

> 在使用maven管理时，开发阶段经常会有很多公共库处于不稳定状态，随时需要修改并发布，可能一天就要发布一次，遇到bug时，甚至一天要发布N次。我们知道，maven的依赖管理是基于版本管理的，对于发布状态的artifact，如果版本号相同，即使我们内部的镜像服务器上的组件比本地新，maven也不会主动下载的。如果我们在开发阶段都是基于正式发布版本来做依赖管理，那么遇到这个问题，就需要升级组件的版本号，可这样就明显不符合要求和实际情况了。但是，如果是基于快照版本，那么问题就自热而然的解决了，而maven已经为我们准备好了这一切。

#### 4.3.2 发布信息介绍

**GroupId**

>项目的唯一标识。Group ID必须满足 [Java包名规范](https://links.jianshu.com/go?to=https%3A%2F%2Fdocs.oracle.com%2Fjavase%2Fspecs%2Fjls%2Fse6%2Fhtml%2Fpackages.html%237.7) ，这意味着是形如：`org.apache.maven`，`org.apache.commons`的格式。Maven不强制此规范，很多遗留的旧项目使用单个单词作为group ID。但是，很难用单个单词作为（新项目的）group ID，并将其提交到Maven中央仓库中。

**ArtifactId**

>**artifactId** 是不带版本号的jar的名字，唯一要求是使用小写字母，且没有特殊符号。如果这是一个第三方的jar包，必须使用其被发布的包名，例如：`maven`，`commons-math`

**Version** 

>  版本号格式是[主版本号].[副版本号].[修复版本号]-[稳定状态]，如：1.0.0-SNAPSHOT。
>
> 1. [主版本号] 是从1开始的整数，表示重大的项目结构和概念调整，一般不会轻易修改该版本号，不同主版本号不承诺能够兼容。
> 2. [副版本号]是从0开始的整数，表示项目的功能特性增加或者BUG修复，同一个[主版本号]下的不同副版本是能够向下兼容的。
> 3. [修复版本号]的只是从0开始的整数，一般只是小的BUG修复，细微功能调整。
> 4. [稳定状态]的可选值有：SNAPSHOT、RC[序号]、RELEASE。
>    1. SNAPSHOT表示开发快照版本，该版本未经过严格测试，可能呢不稳定，不宜用于生产环境；
>    2. RC[序号]表示可选非正式发布版本，比较稳定，但是非正式版本，可能存在问题，可能有多个RC版本，RC序号会有多个；
>    3. RELEASE表示正式发布版本，经过了严格测试，可以用于生产环境，请尽量用该版本作为生产使用。

**Packaging**

> 1. pom ` 统一版本管理，或者编译`
> 2. jar   `提供其他工程依赖使用的jar` 

#### 4.3.3 版本 Snapshot or Release

**Version**

> 版本号相同，比较看后缀

**Snapshot快照仓库**

> snapshot快照仓库用于保存开发过程中的不稳定版本

**Release发布仓库**

> release正式仓库则是用来保存稳定的发行版本
>
> 定义一个组件/模块为快照版本，只需要在pom文件中在该模块的版本号后加上-SNAPSHOT即可
>
> ```xml
> <groupId>cc.mzone</groupId>
> <artifactId>m1</artifactId>
> <version>0.1-SNAPSHOT</version>
> <packaging>jar</packaging>
> ```

#### 4.3.4 统一POM管理

明确产品和各项目所依赖的各类开源信息，避免重复依赖的引入，以及方便对各项目依赖的统一管理。所以就得有统一的依赖配置。在这个配置里统一定义好组件、版本、环境等。

- **管理人**

  框架组或产品组负责人。

- **管理方式** (以maven 为例子)

  新建pom工程，取名parent，类型pom

  - **命名**

    ```xml
    <groupId>com.company.xx</groupId>
    <artifactId>xx-parent</artifactId>
    <version>x.0.0-SNAPSHOT</version>
    <packaging>pom</packaging>
    <name>xx-parent</name>
    ```

  - **加入依赖**

    ```xml
    <dependencyManagement>
      xxx
    </dependencyManagement>
    ```

  - **加入私服**.  (可以放到maven的setting.xml)

    ```xml
    <!-- 发布maven私服 -->
    <distributionManagement>
      <snapshotRepository>
        <id>nexus-snapshots</id>
        <name>nexus-snapshots</name>
        <url>http://${nexus-url}/repository/maven-snapshots</url>
      </snapshotRepository>
      <repository>
        <id>nexus-releases</id>
        <name>nexus-releases</name>
        <url>http://${nexus-url}/repository/maven-releases</url>
      </repository>
    </distributionManagement>
    ```

  - **加入插件**

    ```xml
    <pluginManagement>
      xxx
    </pluginManagement>
    ```

  - **其他**

- **发布**  （以maven为例）

  ```shell
  mvn deploy -Dmaven.test.skip=true
  ```

  Or 

  ```shell
  mvn deploy:deploy-file -Dmaven.test.skip=true -Dfile=D:test-1.0.0.jar -DgroupId=com.test -DartifactId=test -Dversion=1.0.0-SNAPSHOT -Dpackaging=jar -DrepositoryId=nexus-snapshots -Durl=http://${nexus-url}/repository/maven-snapshots
  ```

- **使用方式**（面向开发）

  - 加入pom

    ```xml
    <parent>
      <groupId>com.company.xx</groupId>
      <artifactId>xx-parent</artifactId>
      <version>2.0.0-SNAPSHOT</version>
    </parent>
    ```

  - 引入工程所需依赖(这里无需指定版本了)

    ```xml
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter</artifactId>
    </dependency>
    <!-- 其他 -->
    ```

#### 4.3.5 备注

> maven2会根据模块的版本号(pom文件中的version)中是否带有-SNAPSHOT来判断是快照版本还是正式版本。如果是快照版本，那么在mvn deploy时会自动发布到快照版本库中，而使用快照版本的模块，在不更改版本号的情况下，直接编译打包时，maven会自动从镜像服务器上下载最新的快照版本。如果是正式发布版本，那么在mvn deploy时会自动发布到正式版本库中，而使用正式版本的模块，在不更改版本号的情况下，编译打包时如果本地已经存在该版本的模块则不会主动去镜像服务器上下载。

> 所以，我们在开发阶段，可以将公用库的版本设置为快照版本，而被依赖组件则引用快照版本进行开发，在公用库的快照版本更新后，我们也不需要修改pom文件提示版本号来下载新的版本，直接mvn执行相关编译、打包命令即可重新下载最新的快照库了，从而也方便了我们进行开发。



### 4.4 升级

发布修改的版本到私服，通知各项目组进行更新和升级。

#### 4.4.1 bug修复升级

**文档**

> 1. 描述修复的问题
> 2. 新功能实现
> 3. 组件的使用和注意事项

**版本**

> 依据版本号说明进行命名

#### 4.4.2 功能升级

**文档**

> 1. 功能使用
> 2. 注意事项

**版本**

> 考虑一下是否大版本更新发布





