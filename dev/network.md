

[TOC]

### 网络协议

网络协议为计算机网络中进行数据交换而建立的规则、标准或约定的集合。

#### OSI七层协议

OSI是一个开放性的通信系统互连参考模型，他是一个定义得非常好的协议规范。OSI模型有7层结构

>**下面的图表显示不同的协议在最初OSI模型中的位置：**
>
>7 应用层 例如HTTP、SMTP、SNMP、FTP、Telnet、SIP、SSH、NFS、RTSP、XMPP、Whois、ENRP
>
>6 表示层 例如XDR、ASN.1、SMB、AFP、NCP
>
>5 会话层 例如ASAP、TLS、SSH、ISO 8327 / CCITT X.225、RPC、NetBIOS、ASP、Winsock、BSD sockets
>
>4 传输层 例如TCP、UDP、RTP、SCTP、SPX、ATP、IL
>
>3 网络层 例如IP、ICMP、IGMP、IPX、BGP、OSPF、RIP、IGRP、EIGRP、ARP、RARP、 X.25
>
>2 数据链路层 例如以太网、令牌环、HDLC、帧中继、ISDN、ATM、IEEE 802.11、FDDI、PPP
>
>1 物理层 例如线路、无线电、光纤、信鸽





### TCP&UDP

<img src="../imgs/tcp-udp-yy.jpg" alt="image-20200716151234858" style="zoom:25%;" />

#### TCP

##### 三次握手四次挥手

<img src="../imgs/tcp-3-4.jpg" style="zoom:35%;" />

#### UDP



#### TCP与UDP区别总结：
> 1、TCP面向连接（如打电话要先拨号建立连接）;UDP是无连接的，即发送数据之前不需要建立连接
>
> 2、TCP提供可靠的服务。也就是说，通过TCP连接传送的数据，无差错，不丢失，不重复，且按序到达;UDP尽最大努力交付，即不保证可靠交付
>
> Tcp通过校验和，重传控制，序号标识，滑动窗口、确认应答实现可靠传输。如丢包时的重发控制，还可以对次序乱掉的分包进行顺序控制。
>
> 3、UDP具有较好的实时性，工作效率比TCP高，适用于对高速传输和实时性有较高的通信或广播通信。
>
> 4、每一条TCP连接只能是点到点的;UDP支持一对一，一对多，多对一和多对多的交互通信
>
> 5、TCP对系统资源要求较多，UDP对系统资源要求较少。

#### UDP比TCP优势

UDP以其简单、传输快的优势，在越来越多场景下取代了TCP,如实时游戏。

> （1）网速的提升给UDP的稳定性提供可靠网络保障，丢包率很低，如果使用应用层重传，能够确保传输的可靠性。
>
> （2）TCP为了实现网络通信的可靠性，使用了复杂的拥塞控制算法，建立了繁琐的握手过程，由于TCP内置的系统协议栈中，极难对其进行改进。

采用TCP，一旦发生丢包，TCP会将后续的包缓存起来，等前面的包重传并接收到后再继续发送，延时会越来越大，基于UDP对实时性要求较为严格的情况下，采用自定义重传机制，能够把丢包产生的延迟降到最低，尽量减少网络问题对游戏性造成影响。





https://www.cnblogs.com/aspirant/p/11334957.html



### 跨域

https://www.cnblogs.com/fundebug/p/10329202.html