[TOC]

## React

https://www.jianshu.com/p/519598cb9d81

### useState

```js
const [id, setId] = useState(0)
```









## SPA | SSR |CSR |SEO

### SPA：

- 概念：单页面应用（single page application）
- 页面之间的切换非常快，后端服务器的压力较小，只需要提供api，不需要客户端到底是web端还是手机等，但是首屏打开速度很慢，因为用户首次加载需要先下载SPA框架及应用程序的代码（通过动态地重写页面的部分与用户交互，避免了过多的数据交换，响应速度相对高），然后再渲染页面，而且不利于SEO搜索引擎优化

典型应用框架：AngularJs、React、Vue.js

### SSR：

- 概念：即服务端渲染(Server Side Render)

- 传统的服务端渲染可以使用Java，php 等开发语言来实现，随着 Node.js 和相关前端领域技术的不断进步，前端同学也可以基于此完成独立的服务端渲染

- 过程：浏览器发送请求 -> 服务器运行 react代码生成页面 -> 服务器返回页面 -> 浏览器下载HTML文档 -> 页面准备就绪 即：当前页面的内容是服务器生成好给到浏览器的。

- 有 **SEO 诉求**，用在搜索引擎检索以及社交分享，用在前台类应用。

- **首屏渲染**时长有要求，常用在移动端、弱网情况下。

  >如果你是中后台应用（如 antd pro、管理后台等），请谨慎考虑是否使用 SSR。

### CSR：

- 概念：客户端渲染(Client Side Render)
- 过程：浏览器发送请求 -> 服务器返回空白 HTML(HTML里包含一个root节点和js文件) -> 浏览器下载js文件 -> 浏览器运行react代码 -> 页面准备就绪 即：当前页面的内容是js渲染出来

SEO (搜索引擎优化)，搜索关键词的时候排名，对大多数搜索引擎，不识别JavaScript 内容，只识别 HTML 内容。 （注：原则上可以不用服务端渲染时最好不用，所以如果只有 SEO 要求，可以用预渲染等技术去替代）

### SEO:

概念：搜索引擎优化（Search Engine Optimization）

一种通过了解搜索引擎的运作规则（如何抓取网站页面，如何索引以及如何根据特定的关键字展现搜索结果排序等）来调整网站，以提高该网站在搜索引擎中某些关键词的搜索结果排名,常用技术有[白帽技术](https://link.juejin.im/?target=https%3A%2F%2Fen.wikipedia.org%2Fwiki%2FSearch_engine_optimization)与黑帽技术

之所以说SPA不利于SEQ优化，指的是它对异步数据的支持不足，不会等待异步完成之后在对网页进行抓取

### **区分页面渲染**

打开开发者工具查看网页源代码，网页上的内容是否都在源代码中出现

**特点对比：**

- CSR：首屏渲染时间长，react代码运行在浏览器，消耗的是浏览器的性能
- SSR：首屏渲染时间短，react代码运行在服务器，消耗的是服务器的性能

**为什么要用服务端渲染**

首屏加载时间优化，SSR直接返回生成好内容的HTML，而CSR是先返回空白的HTML，再由浏览器动态加载JavaScript脚本并渲染好后页面才有内容；所以SSR首屏加载更快、减少白屏的时间、用户体验更好



## webpack

### React is not Defined

```javascript

const webpack = require('webpack');
module.exports = {
    ...
    plugins: [
        new webpack.ProvidePlugin({
            "React": "react",
        }) //添加这句
    ]
    ...
};
```

## Antd

当然你可以用create-react-app一把梭来配置antd，但是我更倾向于使用webpack来配置。

首先使用npm安装antd：

```shell
$ npm install antd --save
```


安装babel-plugin-import:

```shell
npm install babel-plugin-import -D
```


配置.babelrc文件中的plugins加上之前的preset-env：

```js
{
    "presets":[
        "@babel/preset-env",
        "@babel/preset-react"
    ],
    "plugins":[[
        "import",{
            "libraryName":"antd",
            "libraryDirectory":"es",
            "style":"css"
        }]
    ]
}
```

然后只需从 antd 引入模块即可，无需单独引入样式。等同于下面手动引入的方式。

// babel-plugin-import 会帮助你加载 JS 和 CSS

```js
import { DatePicker } from 'antd';
```


手动引入

```js
import DatePicker from 'antd/es/date-picker'; // 加载 JS
import 'antd/es/date-picker/style/css'; // 加载 CSS
// import 'antd/es/date-picker/style';         // 加载 LESS
```

### syntax 'classProperties' isn't currently enabled的错误 

解决：安装插件：

```shell
npm i -D @babel/plugin-proposal-class-properties
```


配置插件：

``` json
{
    "presets":[
        "@babel/preset-env",
        "@babel/preset-react"
    ],
    "plugins":[[
        "import",{
            "libraryName":"antd",
            "libraryDirectory":"es",
            "style":"css"
        }],
        ["@babel/plugin-proposal-class-properties"]
    ]
}
```





## vite



