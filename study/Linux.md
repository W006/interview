[TOC]

## 常用命令



## 设置代理

### 可以直接shell执行 也可以写在环境变量里 /etc/profile

```shell
export no_proxy=localhost,127.0.0.1
export http_proxy=http://username:password@host:port
export https_proxy=https://username:password@host:port
export ftp_proxy=http://username:password@host:port
```

### 若用户名称和密码有特殊符号

就是将特殊字符转换成 ASIIC 码形式输入, 以 `% + Hex` 形式(0x忽略).

```
~ : 0x7E,         ! : 0x21    
@ : 0x40,         # : 0x23  
$ : 0x24,         % : 0x25  
^ : 0x5E,         & : 0x26  
* : 0x2A,         ? : 0x3F   
```

例如

```shell
export http_proxy=http://nihao:teshu%ab@host:port
```

改为

```shell
export http_proxy=http://nihao:teshu%25ab@host:port
```

