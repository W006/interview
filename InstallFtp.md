[TOC]

## 1、安装vsftp

### 1.1、安装vsftp，测试安装的vsftpd的版本是：vsftpd.x86_64

```
yum -y install vsftpd
```
### 1.2、修改配置文件

vi /etc/vsftpd/vsftpd.conf
保证下面3项为YES

```properties
anonymous_enable=YES
anon_upload_enable=YES
anon_mkdir_write_enable=YES
```

### 1.3、设置vsftpd开机启动

```shell
systemctl enable vsftpd.service
```

### 1.4、启动并查看vsftpd服务状态，systemctl启动服务成功不会有任何提示，绿色的active表示服务正在运行

```shell
systemctl start vsftpd.service
systemctl status vsftpd.service
```

## 2、本地验证ftp是否可以正常访问

### 2.1、安装ftp

```shell
yum -y install ftp
```

### 2.2、使用anonymous登陆，无需密码

```shell
ftp localhost
```

220表示服务正常，可以登陆；230表示登陆成功。

### 2.3、查看FTP服务器文件夹信息 

## 3、  外部证ftp是否可以正常访问

### 3.1、关闭防火墙（也可以设置防火墙规则，得再百度了）

```shell
systemctl stop firewalld.service
```

为防止机器重启后防火墙服务重新开启，可将防火墙服务永久关闭。

```shell
systemctl disable firewalld.service
```

### 3.2、在window上输入ftp://IP地址，可看到ftp下的目录



## 4、文件读写。

     到上面为止，我们发现ftp目录下并不能读写文件，这是由文件夹权限和selinux引起的。

### 4.1、设置文件夹权限，将pub文件夹的权限设置为777

```shell
chmod 777 -R /var/ftp/pub
```

### 4.1、关闭selinux服务

```shell
vi /etc/selinux/config
# 将SELINUX=enforcing改为：SELINUX=disabled
```

### 4.3、系统重启，让配置生效

```shell
shutdown -r now
```

### 4.4、上传文件



## 5、设置用户登陆

### 5.1 新增用户

useradd neusoft (设置目录权限等等)

### 5.2 设置ftp用户

```shell
vi /etc/vsftpd/vsftpd.conf
#下面是允许某些用户登入的项目  存在user_list文件中的用户不允许登入
userlist_enable=YES
userlist_deny=YES #如果这里改为NO  反过来只能存在user_list文件中的用户允许登入
userlist_file=/etc/vsftpd/user_list
anonymous_enable=NO

#下面是限制用户只能访问自己的主目录  存在chroot_list文件中的用户只能访问自己的主目录
chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd/chroot_list
```

```shell
vi /etc/vsftpd/chroot_list
# 添加用户
neusoft
```



<https://blog.csdn.net/michaelwubo/article/details/82528925>





## 备注

修改ftp的根目录只要修改/etc/vsftpd/vsftpd.conf文件即可：

加入如下几行：

```shell
vi /etc/vsftpd/vsftpd.conf
```

```properties
chroot_local_user=YES
local_root=/var/www/html
anon_root=/var/www/html
```

注：local_root 针对系统用户；anon_root 针对匿名用户。

重新启动服务：

```shell
service vsftpd restart
```

任何一个用户ftp登录到这个服务器上都会chroot到/var/www/html目录下。

