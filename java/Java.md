[TOC]

## 线程

### 1 线程池

#### 核心参数

- corePoolSize 核心线程池数
- maximumPoolSize  最大线程数
- keepAliveTime     非核心线程池存活时间
- workQueue   任务阻塞队列
- timeUnit    超时时间单位
- threadFactory  线程工厂
- rejectedExceptionHandler   超过maximum 和 workQueue 调用的拒绝策略

#### 执行过程

- 提交任务 判断是否大于corePoolSize ，否：产生线程并执行任务，是：放入workQueue
- 若workQueue 满了 ，则判断线程数是否大于maximumPoolSize，否：产生线程并执行任务，是：则执行拒绝策略

#### 拒绝策略	

##### CallerRunsPolicy 

> 当触发拒绝策略，只要线程池没有关闭的话，则使用调用线程直接运行任务。一般并发比较小，性能要求不高，不允许失败。但是，由于调用者自己运行任务，如果任务提交速度过快，可能导致程序阻塞，性能效率上必然的损失较大

##### AbortPolicy 

> 丢弃任务，并抛出拒绝执行 RejectedExecutionException 异常信息。线程池默认的拒绝策略。必须处理好抛出的异常，否则会打断当前的执行流程，影响后续的任务执行。

##### DiscardPolicy 

> 直接丢弃，其他啥都没有

##### DiscardOldestPolicy 

> 当触发拒绝策略，只要线程池没有关闭的话，丢弃阻塞队列 workQueue 中最老的一个任务，并将新任务加入

### 2 Threadloal

####  用途

#### 原理

#### 回收机制



## hash 和 hashcode、equal

#### hash

##### 定义

##### 用处

#### hashcode

### equals 和 hashcode

- equals         是比较对象是相同的关键

- hashcode    is对象存储地址

- 重写equals 为什么要重写 hashcode 

  - 提高集合存取效率

  - java规定

    - 相同的对象 hashcode一定相同

      ```
       想象一下，假如两个Java对象A和B，A和B相等（eqauls结果为true），但A和B的哈希码不同，则A和B存入HashMap时的哈希码计算得到的HashMap内部数组位置索引可能不同，那么A和B很有可能允许同时存入HashMap，显然相等/相同的元素是不允许同时存入HashMap，HashMap不允许存放重复元素。
      ```

    - hashcode 相同 对象不一定相同

      ```
      也就是说，不同对象的hashCode可能相同；假如两个Java对象A和B，A和B不相等（eqauls结果为false），但A和B的哈希码相等，将A和B都存入HashMap时会发生哈希冲突，也就是A和B存放在HashMap内部数组的位置索引相同这时HashMap会在该位置建立一个链接表，将A和B串起来放在该位置，显然，该情况不违反HashMap的使用原则，是允许的。当然，哈希冲突越少越好，尽量采用好的哈希算法以避免哈希冲突
      ```

- 重写equals 要满足一下特点

  - 自反性（reflexive）。对于任意不为null的引用值x，x.equals(x)一定是true。
  - 对称性（symmetric）。对于任意不为null的引用值x和y，当且仅当x.equals(y)是true时，y.equals(x)也是true。
  - 传递性（transitive）。对于任意不为null的引用值x、y和z，如果x.equals(y)是true，同时y.equals(z)是true，那么x.equals(z)一定是true。
  - 一致性（consistent）。对于任意不为null的引用值x和y，如果用于equals比较的对象信息没有被修改的话，多次调用时x.equals(y)要么一致地返回true要么一致地返回false。

**按照一般`hashCode()`方法的实现来说，相等的对象，它们的hash code一定相等**



## 代理

#### 静态代理

#### 动态代理

##### jdk

##### cglib 字节码

### Classloader

### 事务

#### 特性 ACID 4

- 原子性
- 一致性
- 隔离性
- 持久性

#### Spring传播机制  7    propagation_

- required

  ```
  支持当前事务，若无事务，则创建一个事务。Spring默认的事务的传播。 出现异常内外回滚
  ```

- requires_new

  ```
  不管有没有事务，都会启动一个事务，启动后的事务和其他事务隔离，互不影响。 内部异常若是外面回滚异常，则会回滚
  ```

- supports

  ```
  有事务就支持，没有则以非事务方式执行。
  ```

- not_supports

  ```
  以非事务方式执行，若有事务则挂起。
  ```

- mandatory

  ```
  支持当前事务，没有事务抛出异常
  ```

- never

  ```
  以非事务方式执行，若有事务抛出异常
  ```

- nested

  ```
  如果一个活动的事务存在，则运行在一个嵌套的事务中。如果没有活动事务，则按REQUIRED属性执行。它使用了一个单独的事务，这个事务拥有多个可以回滚的保存点。内部事务的回滚不会对外部事务造成影响。它只对DataSourceTransactionManager事务管理器起效。
  ```

#### Spring隔离级别  5     isolation_

```
default（默认数据库）、read_uncommited、read_commited、repeatable_read、serializatable
```

#### 隔离级别  4

- 读未提交  `read_uncommited`

  ```
  导致脏读
  ```

- 读已提交  `read_commited`

  ```
  避免脏读，允许不可重复读和幻读
  ```

- 可重复读   `repeatable_read`

  ```
  避免脏读，不可重复读，允许幻读
  ```

- 串型化   `serializable`

  ```
  串行化读，事务只能一个一个执行，避免了脏读、不可重复读、幻读。执行效率慢，使用时慎重
  ```

#### 数据库默认隔离级别

- 读提交

  ```
  oracle、sqlserver
  ```

- 可重复读

  ```
  mariadb、mysql
  ```


### 慢sql分析

#### sql执行日志



### 设计模式

#### 单例模式

- 饿汉  `提前实例`
  - 静态代码块
  - 直接静态实例
- 懒汉. `用到在实例`
  - 锁
  - 双重检测锁
  - 内部静态类
  - 枚举

### 集合 

#### Collection

##### list

##### set

#### Map

##### hashmap

##### treemap



















