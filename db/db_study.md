## 数据库语言

### 1. 语言范畴

- DDL 

  ```shell
  drop create alter
  ```

- DML

  ```shell
  insert update delete
  ```

- DQL

  ```shell
  select
  ```

- DCL

  ```shell
  grant invoke commit rollback
  ```

### 2. 数据库访问

- odbc
- jdbc
- ado.net
- pdo

