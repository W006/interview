[TOC]

## Sql优化

- 建索引优先考虑where、group by、order by 使用的字段
- 尽量避免使用select *，返回无用的字段会降低查询效率。

>优化方式：使用具体的字段代替*，只返回使用到的字段。

- 尽量避免使用in 和not in，会导致数据库引擎放弃索引进行全表扫描。

>SELECT * FROM t WHERE id IN (2,3)
>
>SELECT * FROM t1 WHERE username IN (SELECT username FROM t2)
>
>优化方式：如果是连续数值，可以用between代替。如下：
>
>SELECT * FROM t WHERE id BETWEEN 2 AND 3
>
>如果是子查询，可以用exists代替。如下：
>
>SELECT * FROM t1 WHERE EXISTS (SELECT * FROM t2 WHERE t1.username = t2.username)

- 尽量避免使用or，会导致数据库引擎放弃索引进行全表扫描。

>SELECT * FROM t WHERE id = 1 OR id = 3
>
>优化方式：可以用union代替or。如下：
>
>SELECT * FROM t WHERE id = 1
>UNION
>SELECT * FROM t WHERE id = 3
>
>（PS：如果or两边的字段是同一个，如例子中这样。貌似两种方式效率差不多，即使union扫描的是索引，or扫描的是全表）
>
>

- 尽量避免在字段开头模糊查询，会导致数据库引擎放弃索引进行全表扫描。

>SELECT * FROM t WHERE username LIKE '%li%'
>
>优化方式：尽量在字段后面使用模糊查询。如下：
>
>SELECT * FROM t WHERE username LIKE 'li%'

- 尽量避免进行null值的判断，会导致数据库引擎放弃索引进行全表扫描。

>SELECT * FROM t WHERE score IS NULL
>
>优化方式：可以给字段添加默认值0，对0值进行判断。如下：
>
>SELECT * FROM t WHERE score = 0

- 尽量避免在where条件中等号的左侧进行表达式、函数操作，会导致数据库引擎放弃索引进行全表扫描。

>SELECT * FROM t2 WHERE score/10 = 9
>
>SELECT * FROM t2 WHERE SUBSTR(username,1,2) = 'li'
>
>优化方式：可以将表达式、函数操作移动到等号右侧。如下：
>
>SELECT * FROM t2 WHERE score = 10*9
>
>SELECT * FROM t2 WHERE username LIKE 'li%'

- 当数据量大时，避免使用where 1=1的条件。通常为了方便拼装查询条件，我们会默认使用该条件，数据库引擎会放弃索引进行全表扫描

>SELECT * FROM t WHERE 1=1
>
>优化方式：用代码拼装sql时进行判断，没where加where，有where加and

### 慢sql分析



## 分区

```
// 创建
CREATE TABLE employees (
    id INT NOT NULL,
    separated DATE NOT NULL DEFAULT '9999-12-31',
    ...
)
PARTITION BY [RANGE|LIST|HASH|LINEAR HASH] [
 	PARTITION p1 VALUES IN (3,5,6,9,17),
    PARTITION p2 VALUES IN (1,2,10,11,19,20),
    |
	PARTITIONS 4;
]

// 删除
ALTER TABLE employees DROP PARTITION p1
```



### 类型

**RANGE分区**：基于属于一个给定连续区间的列值，把多行分配给分区。

>阔以是个连续的，比如id自增划分  
>
>```sql
>CREATE TABLE employees (
>id INT NOT NULL,
>separated DATE NOT NULL DEFAULT '9999-12-31',
>...
>)
>PARTITION BY RANGE (YEAR(separated)) (
>PARTITION p0 VALUES LESS THAN (1991),
>PARTITION p1 VALUES LESS THAN (1996),
>PARTITION p2 VALUES LESS THAN (2001),
>PARTITION p3 VALUES LESS THAN MAXVALUE
>);
>```

**LIST分区**：类似于按RANGE分区，区别在于LIST分区是基于列值匹配一个离散值集合中的某个值来进行选择。LIST分区和RANGE分区很相似，只是分区列的值是离散的，不是连续的。LIST分区使用VALUES IN，因为每个分区的值是离散的，因此只能定义值。

>看成一种枚举
>
>假定有20个音像店，分布在4个有经销权的地区，如下表所示：
>
>====================
>
>地区   商店ID 号
>
>北区   3, 5, 6, 9, 17
>
>东区   1, 2, 10, 11, 19, 20
>
>西区   4, 12, 13, 14, 18
>
>中心区  7, 8, 15, 16
>
>```sql
>CREATE TABLE employees (
>id INT NOT NULL,
>...
>store_id INT
>)
>PARTITION BY LIST(store_id)
>PARTITION pNorth VALUES IN (3,5,6,9,17),
>PARTITION pEast VALUES IN (1,2,10,11,19,20),
>PARTITION pWest VALUES IN (4,12,13,14,18),
>PARTITION pCentral VALUES IN (7,8,15,16)
>);
>```
>
>这使得在表中增加或删除指定地区的雇员记录变得容易起来。例如，假定西区的所有音像店都卖给了其他公司。那么与在西区音像店工作雇员相关的所有记录（行）可以使用查询“ALTER TABLE employees DROP PARTITION pWest；”来进行删除，它与具有同样作用的DELETE（删除）查询“DELETE query DELETE FROM employees WHERE store_id IN (4,12,13,14,18)；”比起来，要有效得多。【要点】：如果试图插入列值（或分区表达式的返回值）不在分区值列表中的一行时，那么“INSERT”查询将失败并报错。例如，假定LIST分区的采用上面的方案，下面的查询将失败：
>
>Sql代码：
>
>```
>INSERT INTO employees VALUES(224, 'Linus', 'Torvalds', '2002-05-01', '2004-10-12', 42, 21);
>```
>
>这是因为“store_id”列值21不能在用于定义分区pNorth, pEast, pWest,或pCentral的值列表中找到。要重点注意的是，LIST分区没有类似如“VALUES LESS THAN MAXVALUE”这样的包含其他值在内的定义。将要匹配的任何值都必须在值列表中找到。
>
>LIST分区除了能和RANGE分区结合起来生成一个复合的子分区，与HASH和KEY分区结合起来生成复合的子分区也是可能的。





**HASH分区**：基于用户定义的表达式的返回值来进行选择的分区，该表达式使用将要插入到表中的这些行的列值进行计算。这个函数可以包含MySQL 中有效的、产生非负整数值的任何表达式。将数据均匀的分布到预先定义的各个分区中，保证每个分区的数量大致相同。

>要使用HASH分区来分割一个表，要在CREATE TABLE 语句上添加一个“PARTITION BY HASH (expr)”子句，其中“expr”是一个返回一个整数的表达式。它可以仅仅是字段类型为MySQL整型的一列的名字。此外，你很可能需要在后面再添加一个“PARTITIONS num”子句，其中num是一个非负的整数，它表示表将要被分割成分区的数量。
>
>```sql
>CREATE TABLE employees (
>id INT NOT NULL, 
>store_id INT
>)
>PARTITION BY HASH(store_id)||PARTITION BY LINEAR HASH(store_id)
>PARTITIONS 4;
>```
>
>如果没有包括一个PARTITIONS子句，那么分区的数量将默认为1。例外：对于NDB Cluster（簇）表，默认的分区数量将与簇数据节点的数量相同，这种修正可能是考虑任何MAX_ROWS设置，以便确保所有的行都能合适地插入到分区中。

**KEY分区**：类似于按HASH分区，区别在于KEY分区只支持计算一列或多列，且MySQL服务器提供其自身的哈希函数。必须有一列或多列包含整数值。KEY分区和HASH分区相似，不同之处在于HASH分区使用用户定义的函数进行分区，KEY分区使用数据库提供的函数进行分区。

>```sql
>CREATE TABLE tk (
>col1 INT NOT NULL,
>col2 CHAR(5),
>col3 DATE
>)
>PARTITION BY LINEAR KEY (col1)
>PARTITIONS 3;
>```
>
>在KEY分区中使用关键字LINEAR和在HASH分区中使用具有同样的作用，分区的编号是通过2的幂（powers-of-two）算法得到，而不是通过模数算法。



## 索引

#### 原理

https://www.cnblogs.com/aspwebchh/p/6652855.html

https://zhuanlan.zhihu.com/p/113917726

### 联合索引 （最左原则）

#### 通俗理解：

利用索引中的附加列，您可以缩小搜索的范围，但使用一个具有两列的索引 不同于使用两个单独的索引。复合索引的结构与电话簿类似，人名由姓和名构成，电话簿首先按姓氏对进行排序，然后按名字对有相同姓氏的人进行排序。如果您知道姓，电话簿将非常有用；如果您知道姓和名，电话簿则更为有用，但如果您只知道名不姓，电话簿将没有用处。

所以说创建复合索引时，应该仔细考虑列的顺序。对索引中的所有列执行搜索或仅对前几列执行搜索时，复合索引非常有用；仅对后面的任意列执行搜索时，复合索引则没有用处。

#### 重点：

多个单列索引在多条件查询时优化器会选择最优索引策略，可能只用一个索引，也可能将多个索引全用上！ 但多个单列索引底层会建立多个B+索引树，比较占用空间，也会浪费一定搜索效率，故如果只有多条件联合查询时最好建联合索引！

#### 最左前缀原则：

顾名思义是最左优先，以最左边的为起点任何连续的索引都能匹配上，
注：如果第一个字段是范围查询需要单独建一个索引
注：在创建联合索引时，要根据业务需求，where子句中使用最频繁的一列放在最左边。这样的话扩展性较好，比如 userid 经常需要作为查询条件，而 mobile 不常常用，则需要把 userid 放在联合索引的第一位置，即最左边

#### 同时存在联合索引和单列索引（字段有重复的），这个时候查询mysql会怎么用索引呢？

这个涉及到mysql本身的查询优化器策略了，当一个表有多条索引可走时, Mysql 根据查询语句的成本来选择走哪条索引；

有人说where查询是按照从左到右的顺序，所以筛选力度大的条件尽量放前面。网上百度过，很多都是这种说法，但是据我研究，mysql执行优化器会对其进行优化，当不考虑索引时，where条件顺序对效率没有影响，真正有影响的是是否用到了索引！

#### 联合索引本质：

当创建**(a,b,c)联合索引时，相当于创建了(a)单列索引**，(a,b)联合索引以及**(a,b,c)联合索引**
想要索引生效的话,只能使用 a和a,b和a,b,c三种组合；当然，我们上面测试过，a,c组合也可以，但实际上只用到了a的索引，c并没有用到！
注：这个可以结合上边的 通俗理解 来思考！

#### 其他知识点：

1、需要加索引的字段，要在where条件中
2、数据量少的字段不需要加索引；因为建索引有一定开销，如果数据量小则没必要建索引（速度反而慢）
3、避免在where子句中使用or来连接条件,因为如果俩个字段中有一个没有索引的话,引擎会放弃索引而产生全表扫描
4、联合索引比对每个列分别建索引更有优势，因为索引建立得越多就越占磁盘空间，在更新数据的时候速度会更慢。另外建立多列索引时，顺序也是需要注意的，应该将严格的索引放在前面，这样筛选的力度会更大，效率更高。 



#### 聚集索引和非聚集索引

聚集索引一个表只能有一个，而非聚集索引一个表可以存在多个

聚集索引存储记录是物理上连续存在，而非聚集索引是逻辑上的连续，物理存储并不连续

聚集索引：物理存储按照索引排序；聚集索引是一种索引组织形式，索引的键值逻辑顺序决定了表数据行的物理存储顺序

非聚集索引：物理存储不按照索引排序；非聚集索引则就是普通索引了，仅仅只是对数据列创建相应的索引，不影响整个表的物理存储顺序.

索引是通过二叉树的数据结构来描述的，我们可以这么理解聚簇索引：索引的叶节点就是数据节点。而非聚簇索引的叶节点仍然是索引节点，只不过有一个指针指向对应的数据块。







## 数据库原理

https://zhuanlan.zhihu.com/p/90693999

https://zhuanlan.zhihu.com/p/141645544

### **1 回滚日志（undo）**

undo log属于逻辑日志，它记录的是sql执行相关的信息。当发生回滚时，InnoDB会根据undo log的内容做与之前相反的工作：对于每个insert，回滚时会执行delete；对于每个delete，回滚时会执行insert；对于每个update，回滚时会执行一个相反的update，把数据改回去。

undo log用于存放数据被修改前的值，如果修改出现异常，可以使用undo日志来实现回滚操作，保证事务的一致性。另外InnoDB MVCC事务特性也是基于undo日志实现的。

因此，undo log有两个作用：**提供回滚和多个行版本控制(MVCC)**。

每次写入数据或者修改数据之前都会把修改前的信息记录到 undo log。

**undo log 有什么作用？**

undo log 记录事务修改之前版本的数据信息，因此假如由于系统错误或者rollback操作而回滚的话可以根据undo log的信息来进行回滚到没被修改前的状态。

总结：
undo log是用来回滚数据的用于保障 未提交事务的原子性

### **2 重做日志（redo）**

redo log重做日志记录的是新数据的备份，属于物理日志。在事务提交前，只要将redo log持久化即可，不需要将数据持久化。当系统崩溃时，虽然数据没有持久化，但是redo log已经持久化。系统可以根据redo log的内容，将所有数据恢复到最新的状态。

redo log包括两部分：一是内存中的日志缓冲(redo log buffer)，该部分日志是易失性的；二是磁盘上的重做日志文件(redo log file)，该部分日志是持久的。

MySQL中redo log刷新规则采用一种称为Checkpoint的机制（利用LSN实现），为了确保安全性，又引入double write机制。

**redo log 有什么作用？**

mysql 为了提升性能不会把每次的修改都实时同步到磁盘，而是会先存到Boffer Pool(缓冲池)里头，把这个当作缓存来用。然后使用后台线程去做**缓冲池和磁盘之间的同步**。

那么问题来了，如果还没来的同步的时候宕机或断电了怎么办？还没来得及执行上面图中红色的操作。这样会导致丢部分已提交事务的修改信息！

所以引入了redo log来记录已成功提交事务的修改信息，并且会把redo log持久化到磁盘，系统重启之后在读取redo log恢复最新数据。

总结：
redo log是用来恢复数据的 用于保障，已提交事务的持久化特性

### **3 MVCC**

MVCC(Multi-Version Concurrency Control)多版本并发控制，可以简单地认为：MVCC就是行级锁的一个变种(升级版)。

事务的隔离级别就是通过锁的机制来实现，只不过隐藏了加锁细节。

在表锁中我们读写是阻塞的，基于提升并发性能的考虑，MVCC一般读写是不阻塞的。

MySQL中MVCC是通过redo log版本链+一致性视图Read-view实现的。

###  4 **mysql锁技术**

当有多个请求来读取表中的数据时可以不采取任何操作，但是多个请求里有读请求，又有修改请求时必须有一种措施来进行并发控制。不然很有可能会造成不一致。
**读写锁**
解决上述问题很简单，只需用两种锁的组合来对读写请求进行控制即可，这两种锁被称为：

共享锁(shared lock),又叫做"读锁"
读锁是可以共享的，或者说多个读请求可以共享一把锁读数据，不会造成阻塞。

排他锁(exclusive lock),又叫做"写锁"
写锁会排斥其他所有获取锁的请求，一直阻塞，直到写入完成释放锁。

![image-20210406211527000](/Users/wen/Library/Application Support/typora-user-images/image-20210406211527000.png)



总结：
通过读写锁，可以做到读读可以并行，但是不能做到写读，写写并行

### **5 事务的实现**

- 原子性：使用 undo log ，从而达到回滚
- 持久性：使用 redo log，从而达到故障后恢复
- 隔离性：使用锁以及MVCC,运用的优化思想有读写分离，读读并行，读写并行
- 一致性：通过回滚，以及恢复，和在并发环境下的隔离做到一致性。

