[TOC]

### 类型

 String、Hash、List、Set、ZSet

### 命令

#### String 一个键最大能存储 512MB

```
set name value
del name
keys * 
config get * 
config loglevel
CONFIG SET CONFIG_SETTING_NAME NEW_CONFIG_VALUE
```

#### Hash 每个 hash 可以存储 $2^{32}$ -1 键值对

```
hmset obj field1 'hello1' field2 'hello2'
hget obj field1
```

#### List 列表最多可存储 $2^{32}$ - 1 元素 双向链表

```
lpush list 1
lpush list 2
rpush list 3 4
lrange list 0 10
lpop list
```

#### Set 集合中最大的成员数为 $2^{32}$ - 1

```
sadd key value
smembers key
srandmember key count
```

#### ZSet 有序集合

Redis zset 和 set 一样也是string类型元素的集合,且不允许重复的成员。不同的是每个元素都会关联一个double类型的分数。redis正是通过分数来为集合中的成员进行从小到大的排序。zset的成员是唯一的,但分数(score)却可以重复。

```
zadd key score value
ZRANGEBYSCORE key 0 1000
zrange key 0 10
```

>Blpop删除，并获得该列表中的第一元素，或阻塞，直到有一个可用
>
>Brpop删除，并获得该列表中的最后一个元素，或阻塞，直到有一个可用
>
>Brpoplpush
>
>Lindex获取一个元素，通过其索引列表
>
>Linsert在列表中的另一个元素之前或之后插入一个元素
>
>Llen获得队列(List)的长度
>
>Lpop从队列的左边出队一个元素
>
>Lpush从队列的左边入队一个或多个元素
>
>Lpushx当队列存在时，从队到左边入队一个元素
>
>Lrange从列表中获取指定返回的元素
>
>Lrem从列表中删除元素
>
>Lset设置队列里面一个元素的值
>
>Ltrim修剪到指定范围内的清单
>
>Rpop从队列的右边出队一个元素
>
>Rpoplpush删除列表中的最后一个元素，将其追加到另一个列表
>
>Rpush从队列的右边入队一个元素
>
>Rpushx从队列的右边入队一个元素，仅队列存在时有效



### Redis 16区

```
select 1
```



### 缓存穿透、缓存雪崩、缓存击穿

#### 缓存穿透

- 问题

> 查询一个不存在的key导致每次都到达数据库层

- 解决办法

>缓存不存在的key 过期时间阔以设置小一点

#### 缓存雪崩

- 问题

>大量key在某一时间过期，或者没有缓存key。导致并发的时候 大量请求达到数据库
>
>避免缓存服务器宕机，导致数据库压力大

- 解决办法

>缓存预热，缓存过期时间交叉
>
>比如不同热度的key 设置不同的缓存过期时间

#### 缓存击穿

- 问题

>缓存击穿，是指一个key非常热点，在不停的扛着大并发，大并发集中对这一个点进行访问，当这个key在失效的瞬间，持续的大并发就穿破缓存，直接请求数据库，就像在一个屏障上凿开了一个洞

- 解决办法

>如果过于热火，设置永不过期。

### CAP理论

- C:consistency(一致性)
- A:avalibility(可用性)
- P:Partition(分区)-tolerence to partition(分区容忍度)

#### 解释CAP

分区：一个分布式系统，网络不通讯，导致连接不通，系统被分割成几个数据区域
原因：数据不连通了，产生数据分区
影响
查还好一点
数据修改时，必须要求数据一致--加锁，实现数据一致性【需求要求数据一致性】
数据修改时，可以数据不一致--不用加锁【需求不要求数据一致性】
分区容忍度
数据的一致性要求高，容忍度高，加锁
数据的一致性要求低，容忍度低，可以不加锁
预期结果，保持数据的一致
可用性
请求在一定时间段内都应该有响应
为了解决锁一直加着
CP理论：【一致性+分区】数据的一致性要求高-加锁
AP理论：【可用性+分区】数据的一致性要求低-不加锁 

#### CAP总结

- 分区是常态，可不避免，三者不可共存
- 可用性和一致性是一对冤家
  - 一致性高，可用性低
  - 一致性低，可用性高

### Redis AP ? CP?

- 单机 CP
- 集群 AP

### Redis 过期策略

#### 定时删除

- 含义：在设置key的过期时间的同时，为该key创建一个定时器，让定时器在key的过期时间来临时，对key进行删除
- 优点：保证内存被尽快释放
- 缺点：
  - 若过期key很多，删除这些key会占用很多的CPU时间，在CPU时间紧张的情况下，CPU不能把所有的时间用来做要紧的事儿，还需要去花时间删除这些key
  - 定时器的创建耗时，若为每一个设置过期时间的key创建一个定时器（将会有大量的定时器产生），性能影响严重
  - 没人用

#### 惰性删除

- 含义：key过期的时候不删除，每次从数据库获取key的时候去检查是否过期，若过期，则删除，返回null。
- 优点：删除操作只发生在从数据库取出key的时候发生，而且只删除当前key，所以对CPU时间的占用是比较少的，而且此时的删除是已经到了非做不可的地步（如果此时还不删除的话，我们就会获取到了已经过期的key了）
- 缺点：若大量的key在超出超时时间后，很久一段时间内，都没有被获取过，那么可能发生内存泄露（无用的垃圾占用了大量的内存）

#### 定期删除

- 含义：每隔一段时间执行一次删除过期key操作
- 优点：
  - 通过限制删除操作的时长和频率，来减少删除操作对CPU时间的占用--处理"定时删除"的缺点
  - 定期删除过期key--处理"惰性删除"的缺点
- 缺点
  - 在内存友好方面，不如"定时删除"
  - 在CPU时间友好方面，不如"惰性删除"
- 难点
  - 合理设置删除操作的执行时长（每次删除执行多长时间）和执行频率（每隔多长时间做一次删除）（这个要根据服务器运行情况来定了）

### 内存淘汰机制

redis 内存淘汰机制有以下几个：

#### noeviction

> 当内存不足以容纳新写入数据时，新写入操作会报错，这个一般没人用吧，实在是太恶心了。

#### allkeys-lru

> 当内存不足以容纳新写入数据时，在键空间中，移除最近最少使用的 key（这个是最常用的）。

#### allkeys-random

> 当内存不足以容纳新写入数据时，在键空间中，随机移除某个 key，这个一般没人用吧，为啥要随机，肯定是把最近最少使用的 key 给干掉啊。

#### volatile-lru

> 当内存不足以容纳新写入数据时，在设置了过期时间的键空间中，移除最近最少使用的 key（这个一般不太合适）。

#### volatile-random

> 当内存不足以容纳新写入数据时，在设置了过期时间的键空间中，随机移除某个 key。

#### volatile-ttl

> 当内存不足以容纳新写入数据时，在设置了过期时间的键空间中，有更早过期时间的 key 优先移除。 

### 高级用法

#### 1. 管道

> 避免每次处理残生`round-trip`
>
> - 文件data.txt
>
>   ```txt
>   SET Key0 Value0
>   SET Key1 Value1
>   SET KeyN ValueN
>   ```
>
> - shell 执行
>
>   ```shell
>   cat data.txt | redis-cli --pipe
>   ```

#### 2.Redis 事务和管道的区别

>Redis 中的事务和 Oracle 中的类似，具有原子性（要么彻底完成，要么不做）和顺序性（按命令请求的先后顺序执行）。
>Redis 中事务和管道的主要区别在于：
>
>- 管道：`client 端行为`，将`多个命令打包一同（而不是先发一个，再接着发下一个命令）发给 server端`，并监听 Socket 返回，通常以阻塞形式等待服务器处理结果；pipe 中的命令一旦到达server根据顺序就会被立即执行；
>- 事务：`server 端行为`，是指 server 对于client 发过来的多个请求命令进行批处理。以 `multi` 命令开启事务，接着逐个接收来自 client 端的命令（放入队列`queue`中），server 会`等待`直至使用命令`exec`，才 开始批处理这段时间区间内所接收的所有命令。

> 栗子

```shell
127.0.0.1:6379> multi
OK
127.0.0.1:6379> set k1 v1
QUEUED
127.0.0.1:6379> set k2 v2
QUEUED
127.0.0.1:6379> set k3 v3
QUEUED
127.0.0.1:6379> exec
1) OK
2) OK
3) OK
127.0.0.1:6379> keys *
1) "Key1"
2) "Key0"
3) "KeyN"
4) "k1"
5) "k3"
6) "k2"
```



#### 3.Redis 分布式锁

首先需要明白这里的“锁”，并`不是真正意义上的锁`。Redis 的 `setnx`命令具有如下作用：

- 当 key 存在时，返回 0，不更新其 value；
- 当 key 不存在时，插入 key-vlaue。

先拿· setnx 来争抢锁`，抢到之后，再用`expire给锁加一个过期时间`防止锁忘记了释放。 这时候对方会告诉你说你回答得不错，然后接着问如果在 setnx 之后执行 expire 之前进程意外crash或者要重启维护了，那会怎么样？ 这时候你要给予惊讶的反馈：唉，是喔，这个`锁就永远得不到释放了`。紧接着你需要抓一抓自己得脑袋，故作思考片刻，好像接下来的结果是你主动思考出来的，然后回答：我记得`set指令有非常复杂的参数`，这个应该是可以同时`把setnx和expire合成一条指令来用`的！对方这时会显露笑容，心里开始默念：摁，这小子还不错。
 jedis.set(String key, String value, String nx, String expx, int time)，这个set()方法一共有五个形参：

第一个为key，我们使用key来当锁，因为key是唯一的。
 第二个为value，我们传的是requestId，很多童鞋可能不明白，有key作为锁不就够了吗，为什么还要用到value？原因就是我们在上面讲到可靠性时，分布式锁要满足第四个条件解铃还须系铃人，通过给value赋值为requestId，我们就知道这把锁是哪个请求加的了，在解锁的时候就可以有依据。requestId可以使用UUID.randomUUID().toString()方法生成。
 第三个为nx，这个参数我们填的是`NX，意思是SET IF NOT EXIST`，即当key不存在时，我们进行set操作；若key已经存在，则不做任何操作；
 第四个为expx，这个参数我们传的是PX，意思是我们要给这个key加一个过期的设置，具体时间由第五个参数决定。
 第五个为time，与第四个参数相呼应，代表key的过期时间。 

#### 4. 分区

分区的目的是让多个 Redis 实例能够同时各自处理一份大数据中的分块（通过增加机器、内存），从而提高Redis的处理性能。分为：

##### 4.1 范围分区

映射一定范围的对象到特定的Redis实例。比如，ID从0到10000的用户会保存到实例R0，ID从10001到 20000的用户会保存到R1，以此类推。这种方式是可行的，并且在实际中使用，不足就是要有一个区间范围到实例的映射表。这个表要被管理，同时还需要各 种对象的映射表，通常对Redis来说并非是好的方法。

##### 4.2 哈希分区

用一个 hash 函数将 key 转换为一个数字；对这个整数取模，将其转化为 0-num 之间的数字（其中 num+1 是 redis 实例数或者分区数）；

即 hash(key) % num。 

#### 5.Redis 集群、分区和哨兵的关系

Redis Sentinal着眼于高可用，在master宕机时会自动将slave提升为master，继续提供服务。
 Redis Cluster着眼于扩展性，在单个redis内存不足时，使用Cluster进行分片存储。

 

### Redis 集群

#### 三种集群模式

##### 主从

主从模式是三种模式中最简单的，在主从复制中，数据库分为两类：主数据库(master)和从数据库(slave)。

>主数据库可以进行读写操作，当读写操作导致数据变化时会自动将数据同步给从数据库
>
>从数据库一般都是只读的，并且接收主数据库同步过来的数据
>
>一个master可以拥有多个slave，但是一个slave只能对应一个master
>
>slave挂了不影响其他slave的读和master的读和写，重新启动后会将数据从master同步过来
>
>master挂了以后，不影响slave的读，但redis不再提供写服务，master重启后redis将重新对外提供写服务
>
>master挂了以后，不会在slave节点中重新选一个master 

**工作机制：**

当slave启动后，主动向master发送SYNC命令。master接收到SYNC命令后在后台保存快照（RDB持久化）和缓存保存快照这段时间的命令，然后将保存的快照文件和缓存的命令发送给slave。slave接收到快照文件和命令后加载快照文件和缓存的执行命令。 复制初始化后，master每次接收到的写命令都会同步发送给slave，保证主从数据一致性。 

**缺点：**

从上面可以看出，master节点在主从模式中唯一，若master挂掉，则redis无法对外提供写服务。

##### Sentinel模式

主从模式的弊端就是不具备高可用性，当master挂掉以后，Redis将不能再对外提供写入操作，因此sentinel应运而生。 sentinel中文含义为哨兵，顾名思义，它的作用就是监控redis集群的运行状况，

> sentinel模式是建立在主从模式的基础上，如果只有一个Redis节点，sentinel就没有任何意义
>
> 当master挂了以后，sentinel会在slave中选择一个做为master，并修改它们的配置文件，其他slave的配置文件也会被修改，比如slaveof属性会指向新的master
>
> 当master重新启动后，它将不再是master而是做为slave接收新的master的同步数据
>
> sentinel因为也是一个进程有挂掉的可能，所以sentinel也会启动多个形成一个sentinel集群
>
> 多sentinel配置的时候，sentinel之间也会自动监控
>
> 当主从模式配置密码时，sentinel也会同步将配置信息修改到配置文件中，不需要担心
>
> 一个sentinel或sentinel集群可以管理多个主从Redis，多个sentinel也可以监控同一个redis
>
> sentinel最好不要和Redis部署在同一台机器，不然Redis的服务器挂了以后，sentinel也挂了 

工作机制：

* 每个sentinel以每秒钟一次的频率向它所知的master，slave以及其他sentinel实例发送一个 PING 命令 

* 如果一个实例距离最后一次有效回复 PING 命令的时间超过 down-after-milliseconds 选项所指定的值， 则这个实例会被sentinel标记为主观下线。 

* 如果一个master被标记为主观下线，则正在监视这个master的所有sentinel要以每秒一次的频率确认master的确进入了主观下线状态

* 当有足够数量的sentinel（大于等于配置文件指定的值）在指定的时间范围内确认master的确进入了主观下线状态， 则master会被标记为客观下线 

* 在一般情况下， 每个sentinel会以每 10 秒一次的频率向它已知的所有master，slave发送 INFO 命令 

* 当master被sentinel标记为客观下线时，sentinel向下线的master的所有slave发送 INFO 命令的频率会从 10 秒一次改为 1 秒一次 

* 若没有足够数量的sentinel同意master已经下线，master的客观下线状态就会被移除；
  若master重新向sentinel的 PING 命令返回有效回复，master的主观下线状态就会被移除 

##### Cluster模式

sentinel模式基本可以满足一般生产的需求，具备高可用性。但是当数据量过大到一台服务器存放不下的情况时，主从模式或sentinel模式就不能满足需求了，这个时候需要对存储的数据进行分片，将数据存储到多个Redis实例中。cluster模式的出现就是为了解决单机Redis容量有限的问题，将Redis的数据根据一定的规则分配到多台机器。cluster可以说是sentinel和主从模式的结合体，通过cluster可以实现主从和master重选功能，所以如果配置两个副本三个分片的话，就需要六个Redis实例。因为Redis的数据是根据一定规则分配到cluster的不同机器的，当数据量过大时，可以新增机器进行扩容。

使用集群，只需要将redis配置文件中的cluster-enable配置打开即可。每个集群中至少需要三个主数据库才能正常运行，新增节点非常方便 

>多个redis节点网络互联，数据共享
>
>所有的节点都是一主一从（也可以是一主多从），其中从不提供服务，仅作为备用
>
>不支持同时处理多个key（如MSET/MGET），因为redis需要把key均匀分布在各个节点上，
>并发量很高的情况下同时创建key-value会降低性能并导致不可预测的行为
>
>支持在线增加、删除节点
>
>客户端可以连接任何一个主节点进行读写



#### 环境

- redis
- ruby环境
- Redis的Ruby驱动redis-xxxx.gem
- 创建Redis集群的工具redis-trib.rb

#### 步骤

#### 1.下载安装Redis 

> https://github.com/MSOpenTech/redis/releases 

#### 2.配置Redis

安装下面配置 配置至少3节点 6380、6381、6382

```properties
port 6380
cluster-enabled yes
cluster-config-file nodes-6380.conf
cluster-node-timeout 15000
## 开启AOF
appendonly yes
## 指定路径
dir ./
## 指定aof文件名称
appendfilename "appendonly.6380.aof"
## 如果cluster-enabled 不为yes， 那么在使用JedisCluster集群代码获取的时候，会报错。
## 那么在创建集群的时候，不会超时。
cluster-node-timeout 15000 
##是为该节点的配置信息，这里使用 nodes-端口.conf命名方法。服务启动后会在目录生成该文件。
cluster-config-file nodes-6380.conf 
```

#### 3. 下载ruby 安装rubygems、ruby-redis、redis-trib.rb

> http://dl.bintray.com/oneclick/rubyinstaller/:rubyinstaller-2.3.3-x64.exe
>
> 下载地址 https://rubygems.org/pages/download， 下载后解压，当前目录切换到解压目录中，如 D:\Program Files\redis\rubygems-2.6.12 然后在命令行执行  ruby setup.rb

##### 3.1 redis

```shell
gem install redis
```

若无网络手动安装

> https://rubygems.org/downloads/redis-3.2.1.gem

安装

```shell
gem install --local redis-3.2.1.gem
```

##### 3.2 安装集群脚本redis-trib

> 下载地址 https://codeload.github.com/beebol/redis-trib.rb/zip/master

```shell
gem install --local redis-trib
```

#### 4. 启动各个节点

> redis-server.exe redis.windows.6380.conf --service-name redis6380

>redis-server.exe redis.windows.6381.conf --service-name redis6381

> redis-server.exe redis.windows.6382.conf --service-name redis6382

#### 5. 创建集群（Clustermo）

```shell
redis-trib.rb create --replicas 1 127.0.0.1:6380 127.0.0.1:6381 127.0.0.1:6382
```

若报错没有ruby的话

```shell
ruby redis-trib.rb create --replicas 1 127.0.0.1:6380 127.0.0.1:6381 127.0.0.1:6382
```

#### 同步原理

**全量同步**
Redis全量复制一般发生在Slave初始化阶段，这时Slave需要将Master上的所有数据都复制一份。具体步骤如下： 
\- 从服务器连接主服务器，发送SYNC命令； 
\- 主服务器接收到SYNC命名后，开始执行BGSAVE命令生成RDB文件并使用缓冲区记录此后执行的所有写命令； 
\- 主服务器BGSAVE执行完后，向所有从服务器发送快照文件，并在发送期间继续记录被执行的写命令； 
\- 从服务器收到快照文件后丢弃所有旧数据，载入收到的快照； 
\- 主服务器快照发送完毕后开始向从服务器发送缓冲区中的写命令； 
\- 从服务器完成对快照的载入，开始接收命令请求，并执行来自主服务器缓冲区的写命令；

**增量同步**
Redis增量复制是指Slave初始化后开始正常工作时主服务器发生的写操作同步到从服务器的过程。 
增量复制的过程主要是主服务器每执行一个写命令就会向从服务器发送相同的写命令，从服务器接收并执行收到的写命令。

**Redis主从同步策略**
主从刚刚连接的时候，进行全量同步；全同步结束后，进行增量同步。当然，如果有需要，slave 在任何时候都可以发起全量同步。redis 策略是，无论如何，首先会尝试进行增量同步，如不成功，要求从机进行全量同步。