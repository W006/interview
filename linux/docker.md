```shell
docker run --name tomcat -p 8081:8081 \
-v /etc/localtime:/etc/localtime:ro \
-v /data/tomcat/webapps:/usr/local/tomcat/webapps \
-v /data/tomcat/logs:/usr/local/tomcat/logs \
-v /data/tomcat/conf:/usr/local/tomcat/conf -d tomcat
```





```shell
docker run -d -p 3306:3306 --name mariadb \
-v /etc/localtime:/etc/localtime:ro \
-v /data/mysql/lib/mysql:/var/lib/mysql \
-v /data/mysql/etc/:/etc/mysql/ \
-e MYSQL_ROOT_PASSWORD=wennn2021 --restart mariadb


docker  run \
--name nacos -d \
-p 8848:8848 \
--privileged=true \
--restart=always \
-e JVM_XMS=256m \
-e JVM_XMX=256m \
-e MODE=standalone \
-e PREFER_HOST_MODE=hostname \
-v /data/nacos/logs:/home/nacos/logs \
-v /data/nacos/init.d/custom.properties:/home/nacos/init.d/custom.properties \
nacos/nacos-server


```

