[TOC]



###  Save or Load

#### Save

```shell
docker save alpine:latest > alpine.tar
# 
docker save -o alpine:lateste alpine.tar
```



 

**删除 dangling 镜像：**

```
docker image prune
```

或者：

```
docker rmi $(docker images -f "dangling=true" -q)
```

如果镜像被容器引用了，是不能直接删除的，需要先删除容器。

```
# 删除容器
docker rm $(docker ps -a | grep "Exited" | awk '{print $1}')
# 删除 images
docker rmi $(docker images | grep "none" | awk '{print $3}')
```

或者：

```
docker ps -a | grep "Exited" | awk '{print $1}'| xargs docker rm
docker images | grep none | awk '{print $3}'| xargs docker rmi
```

