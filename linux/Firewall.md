[TOC]

## 防火墙的常用操作centos7+

### centos7启动防火墙

```shell
systemctl start firewalld.service
```

### centos7停止防火墙/关闭防火墙

```shell
systemctl stop firewalld.service
```

### centos7重启防火墙

```shell
systemctl restart firewalld.service
```

### 设置开机启用防火墙

```shell
systemctl enable firewalld.service
```

### 设置开机不启动防火墙

```shell
systemctl disable firewalld.service

firewall-cmd --zone=public --add-port=80/tcp --permanent
```

### 说明:
#### –zone #作用域
#### –add-port=80/tcp #添加端口，格式为：端口/通讯协议
#### –permanent 永久生效，没有此参数重启后失效

#### 多个端口:

```shell
firewall-cmd --zone=public --add-port=80-90/tcp --permanent
```

### centos7以下使用netstat -ant,7使用ss

```shell
ss -ant
```

### centos7查看防火墙所有信息

```shell
firewall-cmd --list-all
```

### centos7查看防火墙开放的端口信息

```shell
firewall-cmd --list-ports
```

### 删除

```shell
firewall-cmd --zone=public --remove-port=80/tcp --permanent
```





### 开启防火墙

```
systemctl start firewalld.service
```

### 防火墙开机启动

```
systemctl enable firewalld.service
```

### 关闭防火墙

```
systemctl stop firewalld.service
```

### 查看防火墙状态

```
firewall-cmd --state
```

### 查看现有的规则

```
iptables -nL
```

### 重载防火墙配置

```
firewall-cmd --reload
```

### 添加单个单端口

**`firewall-cmd --permanent --zone=public --add-port=81/tcp`**

### 添加多个端口

**`firewall-cmd --permanent --zone=public --add-port=8080-8083/tcp`**

### 删除某个端口

**`firewall-cmd --permanent --zone=public --remove-port=81/tcp`**

# 针对某个 IP开放端口

```
firewall-cmd --permanent --add-rich-rule="rule family="ipv4" source address="192.168.142.166" port protocol="tcp" port="6379" accept"`
 `firewall-cmd --permanent --add-rich-rule="rule family="ipv4" source address="192.168.0.233" accept"
```

### 删除某个IP

```
firewall-cmd --permanent --remove-rich-rule="rule family="ipv4" source address="192.168.1.51" accept"
```

### 针对一个ip段访问

```
firewall-cmd --permanent --add-rich-rule="rule family="ipv4" source address="192.168.0.0/16" accept"`
 `firewall-cmd --permanent --add-rich-rule="rule family="ipv4" source address="192.168.1.0/24" port protocol="tcp" port="9200" accept"
```

### 添加操作后别忘了执行重载

```
firewall-cmd --reload
```



作者：风亡小窝
链接：https://www.jianshu.com/p/e52c41be32ac
来源：简书
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
