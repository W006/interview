## 1、简介 
本文主要对ntp服务在局域网内的安装和配置做了简要的介绍。 

## 2、服务器端安装 
tar zxvf ntp-4.2.6p5.tar.gz 
cd ntp-4.2.6p5 
./configure --prefix=/usr/ntp --enable-all-clocks --enable-parse-clocks 
make 
make install 
该部分是在时间同步服务中充当服务器的机器上要进行的安装，其他机器将会行这台机器上获取时间，实现同步。 

## 3、服务器端配置 
（1） 允许客户机的配置 
修改ntp.conf配置文件 
vi /etc/ntp.conf 

①、第一种配置：允许任何IP的客户机都可以进行时间同步 
将“restrict default kod nomodify notrap nopeer noquery”这行修改成： 
restrict default nomodify 

②、第二种配置：只允许10.15.62.*网段的客户机进行时间同步 
在restrict default nomodify notrap noquery（表示默认拒绝所有IP的时间同步）之后增加一行： 
restrict 10.15.62.0 mask 255.255.255.0 nomodify    

（2） 同步时间设置 
①通过网络同步时间 
\# Use public servers from the pool.ntp.org project. 
\# Please consider joining the pool (http://www.pool.ntp.org/join.html). 
server 0.centos.pool.ntp.org 
server 1.centos.pool.ntp.org 
server 2.centos.pool.ntp.org 

②使用本地时间 
\# Undisciplined Local Clock. This is a fake driver intended for backup 
\# and when no outside source of synchronized time is available. 
server 127.127.1.0   # local clock 
fudge  127.127.1.0 stratum 10 

## 4、启动ntp服务器 
(1)通过bin目录下的脚本启动 
/usr/local/ntp/bin/ntpd -c /etc/ntp.conf -p /tmp/ntpd.pid 

(2)通过service工具启动 
service ntpd status 
service ntpd start 
service ntpd stop 

## 5、客户端配置 
（1）安装ntpdate 
tar zxvf ntpdate.tar.gz 
然后把解压出的ntpdate可执行脚本拷贝到/usr/bin目录或者是其他系统自动查询的可执行程序的目录即可。 
（2）配置 
vim /etc/crontab 
如下配置成每小时同步一次 
\# Example of job definition: 
\# .---------------- minute (0 - 59) 
\# |  .------------- hour (0 - 23) 
\# |  |  .---------- day of month (1 - 31) 
\# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ... 
\# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat 
\# |  |  |  |  | 
\# *  *  *  *  * user-name command to be executed 
  1  *  *  *  * root ntpdate 10.15.62.70 && hwclock -w 

